#include "tensorflow/core/framework/op.h"
#include "tensorflow/core/framework/shape_inference.h"
#include "tensorflow/core/framework/op_kernel.h"

using namespace tensorflow;

// void set_output(int idx, ShapeHandle shape) { outputs_[idx] = shape; }
// ShapeHandle input(int idx) const { return inputs_[idx]; }
REGISTER_OP("DoubleEach")
.Input("to_double : int32")
.Output("double : int32")
.SetShapeFn([](::tensorflow::shape_inference::InferenceContext* c){
		c->set_output(0, c->input(0));
		return Status::OK();
		});

/*

PS: Notice the use of - 
1. :: operator.
2. explicit keyword. 

 */

class DoubleEachOp : public OpKernel {
	public:
		explicit DoubleEachOp(OpKernelConstruction* context) : OpKernel(context) {}

		void Compute(OpKernelContext* context) override {

			// Grab the input tensor
			const Tensor& input_tensor = context->input(0);
			auto input = input_tensor.flat<int32>();

			// Create an output tensor
			Tensor* output_tensor = NULL;
			OP_REQUIRES_OK(context, context->allocate_output(0, input_tensor.shape(),
						&output_tensor));
			auto output = output_tensor->flat<int32>();

			// Double each element.
			const int N = input.size();
			if(N == 0){
				return;
			}
			for (int i = 0; i < N; i++) {
				output(i) = 2 * input(i);
			}

		}
};

REGISTER_KERNEL_BUILDER(Name("DoubleEach").Device(DEVICE_CPU), DoubleEachOp);
