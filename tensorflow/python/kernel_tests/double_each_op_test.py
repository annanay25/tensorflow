import tensorflow as tf

class DoubleEachTest(tf.test.TestCase):
	def testDoubleEach(self):
		double_each_module = tf.load_op_library('double_each.so')
		with self.test_session():
			result = double_each_module.double_each([1,2,3,4])
			self.assertAllEqual(result.eval(), [2,4,6,8])

if __name__ == "__main__":
	tf.test.main()

